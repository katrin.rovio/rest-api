# REST API

This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## 1. Getting started

Clone the repository:

```bash
git clone git@gitlab.com:katrin.rovio/rest-api.git
```
## 2. Initialize npm

Run the following command to initialize npm in the project directory. 

*As a result this will create a package.json file with default values*:

```bash
npm init -y
```

## 3. Install Express with the dependencies

This will install Express and add it as a dependency into the package.json file

```bash
npm install express --save
```

## 4. Create .gitignore

Create a .gitignore file to specify files and directories that should be ignored by Git:

`touch .gitignore`

*Add files like node_modules/ and .env to this file to prevent them from being tracked by Git.*

## 5. Check git status

Check the status of your Git repository to see which files are staged for commit and which are not:

`git status`

## 6. Stage changes

Stage all changes in your project directory for the next commit:

`git add .`

## 7. Commit Changes
Commit the staged changes with a descriptive message:

```bash
git commit -m "Initial commit"
```

## 8. Push to Remote Repository

Push your committed changes to the set up remote repository (in our example on GitHub):

```bash
git push
```

*This will push changes to the default branch of the remote repository.*

---
---
## Project Setup Commands


| Command                      | Description                                 |
|------------------------------|---------------------------------------------|
| `npm init -y`                | Initializes npm in the project directory.   |
| `npm install express --save` | Installs Express as a dependency.           |
| `touch .gitignore`           | Creates a .gitignore file.                   |              |
| `git status`                  | Check the status of your Git repository.     |
| `git add .`                  | Stages all changes for the next commit.     |
| `git commit -m "Initial commit"` | Commits the staged changes.               |
| `git push`                   | Pushes the committed changes to the remote repository. |

---
---

# Running the Application

To use the API, send HTTP requests to the appropriate endpoints. Here's an example:

Start the REST API service:

`node app.js`

Start the server with file watching enabled:

`node --watch app.js`

---
---

# Testing the API

You can test the API using tools like curl or by accessing endpoints in your browser.

*For example, to access the root endpoint:*

```bash
curl http://127.0.0.1:3000/
```

---
---

## Development Commands

| Command                      | Description                                   |
|------------------------------|-----------------------------------------------|
| `node app.js`                | Starts the application server.                |
| `node --watch app.js`        | Starts the server with file watching enabled. |
| `curl http://127.0.0.1:3000/` | Sends a GET request to the root endpoint.   |

---
---

# Finish Readme.md

This README.md file has been updated to provide comprehensive documentation for the project. Changes have been made using the Markdown extension in Visual Studio Code.

# To-do List

- [x] Clone repository
- [x] Express marked as dependency
    - [x] Initialize npm
    - [x] Install Express
- [x] node_modules ignored successfully
    - [x] Create .gitignore
- [x] Check status
- [x] Stage Changes
- [x] Commit Changes
- [x] Push to Remote Repository
- [x] Only one default branch e.g., master or main, not both
- [x] One pure JavaScript function documented properly with JSDoc
- [x] Readme modified to match and describe the project
- [ ] Return your project link (repository).
---
---

### Mission complete - ... well ... maybe

![Läheltä piti!](https://scontent-hel3-1.xx.fbcdn.net/v/t1.6435-9/92927064_10215538673811399_6216746332376268800_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=5f2048&_nc_ohc=SbjwrSCtUNsAb4Z_vlI&_nc_ht=scontent-hel3-1.xx&oh=00_AfCLVCmMc-n72vr6Sj0axzGQtaU8SQrss_ICwW3V8c96qA&oe=66408C93)

---
---

<small>*For more detailed documentation, please visit our [Project documentation](https://gitlab.com/katrin.rovio/rest-api).*</small>

---
---